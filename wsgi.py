#!/usr/bin/env python3

# Python packages
import os

# Hitch packages
from hitchhike import create_app

# Create the app
app = create_app()

if __name__ == "__main__":
    app.run()
