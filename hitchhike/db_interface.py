# db_interface

# flask packages
from flask import Blueprint

# flask structures
from flask import g

# HITCH structures
from .      import db
bp = Blueprint(__name__, 'db_bp')

def get_session():
    if 'db' not in g:
        g.db = db.session
    return g.db

@bp.teardown_app_request
def return_connection_to_pool(self):
    db_session = g.pop('db', None)
    if db_session is not None:
        db_session.close()

def retrieve_query(query, args, fetchstyle):
    session = get_session()
    result = session.execute(query, args)
    if fetchstyle == 'one':
        return result.fetchone()
    else:
        return result.fetchall()


def retrieve_one(query, args):
    return retrieve_query(query, args, 'one')

def retrieve_all(query, args):
    return retrieve_query(query, args, 'all')

def commit(query, args):
    session = get_session()
    result = session.execute(query, args)
    session.commit()
