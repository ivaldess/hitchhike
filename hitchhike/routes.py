# Python packages
import  os, random, string, json
from    os.path import join, dirname, realpath

# Flask packages
from flask              import Blueprint # type of object
from flask              import render_template, redirect, url_for, flash # flask methods
from flask              import request, current_app  # specific pieces of the application
from flask              import session # flask session
from werkzeug.utils     import secure_filename
from werkzeug.security  import check_password_hash
from functools          import wraps
import boto3

# Hitch packages
from .hitchSQL import HitchSQL
from .         import spaces_interface

bp = Blueprint(__name__, "route_bp")
database = HitchSQL()
space_location      = None # This is set by create_app in __init__
space_location_pdf  = None # This is set by create_app in __init__

def allowed_file(filename):
    allowed_types = ['pdf', 'png', 'jpg', 'jpeg']

    return '.' in filename and filename.rsplit('.',1)[1].lower() in allowed_types

@bp.route('/')
def home():
    return render_template("landing_page.html")

@bp.route('/suggestive_bar.js')
def suggestive_bar():
    cities = database.getAllCities()

    return render_template("suggestive_bar.js", cities=cities)

@bp.route('/keyword_suggestions.js/<string:city>')
def keyword_suggestions(city):
    keywords = database.getKeywords(city)
    return render_template("keyword_suggestions.js", keywords=keywords)


@bp.route('/keyword_suggestions.js')
def all_keyword_suggestions():
    all_keywords = []
    keywords = database.getAllKeywords()
    [all_keywords.append("\"" + keyword[0] + "\"") for keyword in keywords]
    keyword_string = ','.join(all_keywords)
    return render_template("keyword_suggestions.js", keywords=keyword_string)


@bp.route('/suggestive_country.js')
def suggestive_country():
    cities      = ["Guatemala", "Mexico", "United States", "Morrocco"]
    all_cities  = []
    for city in cities:
        all_cities.append("\"" + city[0] + "\"")
    city_string = ','.join(all_cities)

    return render_template("suggestive_country.js", cities=city_string)

@bp.route('/suggestive_state.js')
def suggestive_state(): 
    return render_template("suggestive_state.js")

@bp.route('/suggestive_city.js')
def suggestive_city():
    stateDict   = {}
    entries     = database.getAllCities();
    for entry in entries.split('\",\"'):
        tokens  = entry.split(",")
        city    = tokens[0]
        state   = tokens[1]
        city = city.replace('\"', '')
        state= state.replace(' ', '')
        if  state not in stateDict:
            stateDict[state] = [city]
        elif city not in stateDict[state]:
            stateDict[state].append(city)
    stateDict   = json.dumps(stateDict) 
    return render_template("suggestive_city.js", stateDict=stateDict)

@bp.route('/city_offerings', methods=['POST'])
def city_offerings():
    city = request.form["city"]

    return redirect("/{}".format(city))

@bp.route('/<string:city>', methods=['GET'])
def city_offerings_tiles(city):
    if "search_terms" in session and city in session["search_terms"]:
        new_search = session["search_terms"][city]
        if len(new_search) == 0:
            offerings = database.getBusinessesByCity(city)
        else:
            offerings = database.limitSearch(new_search, city)
        return render_template("city_businesses.html", data=offerings, city=city, searched=new_search, space_location=space_location, space_location_pdf=space_location_pdf)
    else:
        offerings = database.getBusinessesByCity(city)
        return render_template("city_businesses.html", data=offerings, city=city, searched=False, space_location=space_location, space_location_pdf=space_location_pdf)

@bp.route('/<string:city>', methods=['POST'])
def city_offerings_limit_search(city):
    search      = request.form.get("search", None)   
    # Regular search
    if search:
        search  = search.split(',')
        search  = [ word.strip() for word in search ]
    # Clear Search Button was pressed
    else: 
        session.pop("search_terms", None)
        offerings = database.getBusinessesByCity(city)
        return render_template("city_businesses.html", data=offerings, city=city, searched=False, space_location=space_location, space_location_pdf=space_location_pdf)

    if "search_terms" in session and city in session["search_terms"]:
        session["search_terms"][city].extend(search)
        search = session["search_terms"][city]
    else:
        session["search_terms"] = {}
        session["search_terms"][city] = search
    
    session.modified = True
    offerings   = database.limitSearch(search, city)

    return render_template("city_businesses.html", data=offerings, city=city, searched=search, space_location=space_location, space_location_pdf=space_location_pdf)

@bp.route('/<string:city>/<string:remove_term>', methods=['GET'])
def remove_search_term(city, remove_term):
    if "search_terms" in session:
        search_terms = session["search_terms"][city]
        try:
            new_search_terms = session["search_terms"][city]
            new_search_terms.remove(remove_term)
            session["search_terms"][city] = new_search_terms
            session.modified = True
        except:
            session["search_terms"][city] = []
    return redirect(url_for('hitchhike.routes.city_offerings_tiles', city=city))


@bp.route('/<int:businessID>', methods=['GET'])
def business_page(businessID):
    business_info   = database.getBusinessData(businessID)
    keywords        = database.getBusinessKeywords(businessID)
    city            = database.getBusinessCity(businessID)

    return render_template("product_page.html", data=business_info, keywords=keywords, city=city, space_location=space_location, space_location_pdf=space_location_pdf)

@bp.route('/business_owner', methods=['GET'])
def verify_business_get():
    defaults = session.pop("inputs", None)
    uploaded = session.pop("successfully_uploaded", None)
    if defaults:
        keywords = defaults['keywords']
        defaults['keywords'] = [ keyword.strip() for keyword in keywords.split(',')]

    return render_template("business_owner.html", data=None, defaults=defaults, uploaded=uploaded, space_location=space_location, space_location_pdf=space_location_pdf)

@bp.route('/business_owner', methods=['POST'])
def verify_business():
    pdf     = request.files['pdf']
    logo    = request.files['logo']

    if pdf.filename == '' or logo.filename == '':
        flash('Missing Selected Files! Add a logo and pdf and try again')
        return redirect('/business_owner')

    if pdf and allowed_file(pdf.filename) and logo and allowed_file(logo.filename):
        pdf_name        = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
        pdf_name        = pdf_name + ".pdf"
        pdf_location    = join(current_app.config["PDF_FOLDER"], pdf_name)
        pdf.save(pdf_location)
        logo_name       = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
        logo_location   = join(current_app.config["LOGO_FOLDER"], logo_name)
        logo.save(logo_location)

        spaces_interface.upload_pdf(pdf_location, 'pdfs', pdf_name)
        spaces_interface.upload_image(logo_location, 'logos', logo_name)

    result              = request.form
    restaurant          = result.copy()
    restaurant["pdf"]   = pdf_name
    restaurant["logo"]  = logo_name
    # Remove all unwanted characters (") from fields
    for key in restaurant.keys():
        restaurant[key] = restaurant[key].replace("\"", "\\\"")
        restaurant[key] = restaurant[key].replace("\'", "\\\'")
    session["inputs"]   = restaurant.copy()

    return render_template("business_owner.html", data=restaurant, space_location=space_location, space_location_pdf=space_location_pdf)

@bp.route('/confirm_business', methods=['GET'])
def business_confirmed():
    business = session.pop("inputs", None)
    database.addBusiness(business)
    session["successfully_uploaded"] = True
    flash('Success!')

    return redirect('/business_owner')

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            print("you are not logged in")
            return redirect(url_for('hitchhike.routes.login_page'))

    return wrap

# Route for handling the login page logic
@bp.route('/login', methods=["GET","POST"])
def login_page():
    error= ''
    if request.method == "POST":
        attempted_username = request.form['username']
        attempted_password = request.form['password']
        real_password = database.getPassword()
        if attempted_username == "admin" and check_password_hash(real_password, attempted_password):
            session['logged_in'] = True
            return redirect(url_for('.admin_page'))
        else:
            error = "Invalid credentials. Try Again."
            return render_template("login.html", error=error)

    return render_template("login.html")



@bp.route('/admin_page', methods= ['GET', 'POST'])
@login_required
def admin_page():
    businesses   = database.viewUnconfirmed()
    if request.method == 'POST':
        biz_id= request.form['biz_id']
        if request.form['submit_button'] == 'Confirm':
            database.confirmBusiness(biz_id)
        elif request.form['submit_button'] == 'Delete':
            database.deleteBusiness(biz_id)
        else:
            pass # unknown

        businesses   = database.viewUnconfirmed()
        return render_template("admin_page.html", data=businesses, space_location=space_location, space_location_pdf=space_location_pdf)

    elif request.method == 'GET':
        return render_template("admin_page.html", data=businesses, space_location=space_location, space_location_pdf=space_location_pdf)
