# spaces_interface, for accessing digitalocean spaces

# Python packages
import os
import copy

# flask packages
from flask import Blueprint
import boto3

# flask structures
from flask import current_app, g

# Snips structures
from .      import db
bp = Blueprint(__name__, 'spaces_bp')

EXTRA_ARGS  = {'ACL': 'public-read'}

def get_space_client():
    if 'spaces_client' not in g:
        g.spaces_session    = boto3.session.Session()
        region_name         = current_app.config['SPACES_REGION_NAME']
        endpoint_url        = current_app.config['SPACES_ENDPOINT_URL']
        access_key_id       = current_app.config['SPACES_ACCESS_KEY_ID']
        secret_access_key   = current_app.config['SPACES_SECRET_ACCESS_KEY']
        g.space_name        = current_app.config['SPACES_SPACE_NAME']

        g.spaces_client = g.spaces_session.client('s3', region_name=region_name, endpoint_url=endpoint_url, aws_access_key_id=access_key_id, aws_secret_access_key=secret_access_key)
    return (g.spaces_client, g.space_name)

@bp.teardown_app_request
def delete_spaces_session_client(self):
    space_session   = g.pop('spaces_session', None)
    space_client    = g.pop('spaces_client', None)
    space_name      = g.pop('space_name', None)

def upload_image(file_location_on_disk, file_folder_cloud, file_name_cloud):
    upload_file(file_location_on_disk, file_folder_cloud, file_name_cloud, None)

def upload_pdf(file_location_on_disk, file_folder_cloud, file_name_cloud):
    upload_file(file_location_on_disk, file_folder_cloud, file_name_cloud, 'application/pdf')

def upload_file(file_location_on_disk, file_folder_cloud, file_name_cloud, mime_type):
    try:
        file_location_cloud = '/'.join([file_folder_cloud, file_name_cloud])
        extra_args = copy.deepcopy(EXTRA_ARGS)
        if mime_type:
            extra_args['ContentType'] = mime_type

        client, space_name = get_space_client()

        client.upload_file(file_location_on_disk, space_name, file_location_cloud, ExtraArgs=extra_args)
        os.remove(file_location_on_disk)
    except:
        print('Upload Failed: {}, {}, {}'.format(file_location_on_disk, file_folder_cloud, file_name_cloud))
