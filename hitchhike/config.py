from os.path import join, dirname, realpath
import redis
from os import environ

class DefaultConfig():

    # File Config
    PDF_FOLDER  = join(dirname(realpath(__file__)), 'static/pdfs')
    LOGO_FOLDER = join(dirname(realpath(__file__)), 'static/logos')

    # Flask-Session
    SESSION_TYPE = environ.get('SESSION_TYPE')

    # PDF VIEWER
    PDF_VIEW_PREFIX = 'https://drive.google.com/viewerng/viewer?pid=explorer&efh=false&a=v&chrome=false&embedded=true&url='
