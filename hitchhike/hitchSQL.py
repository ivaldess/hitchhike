# Hitch packages
from . import db_interface

class HitchSQL():

    def getBusinessesByCity(self, city_name):
        query   = 'select businessID from cities where name = :city_name'
        args    = {'city_name' : city_name}
        all_ids = db_interface.retrieve_all(query, args)

        return self.getBusinessesWithIds([id[0] for id in all_ids])

    def getBusinessData(self, businessID):
        query   = 'select * from businesses where businessID = :biz_id'
        args    = {'biz_id' : businessID}

        result = db_interface.retrieve_one(query, args)
        if not result:
            return None
   
        value = {"business_id": result[0], "name": result[1], "website": result[2], "pdf": result[3], "description": result[4], "address": result[5], "phone_number": result[6], "logo": result[7] }
        return value
        

    def getBusinessKeywords(self, businessID):
        query   = 'select keyword from businessKeywords where businessID = :biz_id'
        args    = {'biz_id' : businessID}

        keywords = db_interface.retrieve_all(query, args)
        if not keywords:
            return []
        else:
            return [keyword[0] for keyword in keywords ]
    
    def getBusinessCity(self, businessID):
        query   = 'select name from cities where businessID = :biz_id'
        args    = {'biz_id' : businessID}

        result = db_interface.retrieve_one(query, args)
        if not result:
            return None
        else:
            return db_interface.retrieve_one(query, args)[0]

    def getBusinessNameLogo(self, businessID):
        query       = 'select name, logo from businesses where businessID = :biz_id '
        args        = {'biz_id' : businessID}
        name, logo  = db_interface.retrieve_one(query, args)
        return (name, logo)

    def getBusinessesWithIds(self, all_ids):
        businesses = []
        for one_id in all_ids:
            business    = {}

            business["id"]                      = one_id
            business['keywords']                = self.getBusinessKeywords(one_id)
            business['name'], business['logo']  = self.getBusinessNameLogo(one_id)
            businesses.append(business)
        return businesses

    def getKeywords(self, city):
        allKeywords= []
        businesses = self.getBusinessesByCity(city)
        for business in businesses:
            allKeywords.append(business['name'])
            for keyword in business['keywords']:
                if keyword not in allKeywords:
                    allKeywords.append(keyword)

        all_keywords = []
        for keyword in allKeywords:
            all_keywords.append("\"" + keyword + "\"")
        keyword_string = ','.join(all_keywords)

        return keyword_string

    def getAllKeywords(self):
        query = 'select distinct keyword from businessKeywords'
        return db_interface.retrieve_all(query, None)


    def getAllCities(self):
        query   = 'select distinct name from cities'
        cities  = db_interface.retrieve_all(query, None)
        all_cities = []
        for city in cities:
            all_cities.append("\"" + city[0] + "\"")
        city_string = ','.join(all_cities)
        return city_string

    def limitSearch(self, search, city):
        format_string = ','.join([':arg{}'.format(num) for num in range(len(search)) ])
        query = 'select cities.businessID from cities, businessKeywords where cities.businessID = businessKeywords.businessID and name = :city and keyword in ({})'.format(format_string)
        args            = { 'city' : city }
        search_arg_list = tuple(search)
        for i in range(len(search)):
            args['arg{}'.format(i)] = search_arg_list[i]

        all_ids = db_interface.retrieve_all(query, args)
        all_ids = set([ int(id[0]) for id in all_ids])

        query = 'select businesses.businessID from cities, businesses where cities.businessID = businesses.businessID and cities.name = :city and businesses.name in ({})'.format(format_string)
        new_ids = db_interface.retrieve_all(query, args)
        if new_ids:
            [all_ids.add(new_id[0]) for new_id in new_ids]

        return self.getBusinessesWithIds(all_ids)

    def addBusiness(self, r):
        query = "insert into unconfirmed_businesses (name, website, pdf, address, city, state, country, phone_number, logo, keywords, email) values (:name, :website, :pdf, :address, :city, :state, :country, :phone_number, :logo, :keywords, :email)"
        args  = {
            'name'          : r['name'],
            'website'       : r['website'],
            'pdf'           : r['pdf'],
            'address'       : r['address'],
            'city'          : r['city'],
            'state'         : r['state'],
            'country'       : r['country'],
            'phone_number'  : r['phone_number'],
            'logo'          : r['logo'],
            'keywords'      : r['keywords'], 
            'email'         : r['email']
        }

        db_interface.commit(query, args)

    def viewUnconfirmed(self):
        query = "select * from unconfirmed_businesses"
        return db_interface.retrieve_all(query, None)

    def confirmBusiness(self, businessID):
        query       = 'select * from unconfirmed_businesses where id = :biz_id '        # Get business info
        args        = {'biz_id' : businessID}
        b           = db_interface.retrieve_one(query, args)
                                                                                        # Insert business into database
        query = "insert into businesses (name, website, pdf, address, phone_number, logo, keywords, email) \
                values (:name, :website, :pdf, :address, :phone_number, :logo, :keywords, :email)"
        city    = b['city']
        state   = b['state']
        country = b['country']
        fullAdd = city + ", " + state + ", " + country if state != "N/A" else city + ", " + country 
        args  = {
            'name'          : b['name'],
            'website'       : b['website'],
            'pdf'           : b['pdf'],
            'address'       : b['address'] +  "," + fullAdd,
            'phone_number'  : b['phone_number'],
            'logo'          : b['logo'],
            'keywords'      : b['keywords'], 
            'email'         : b['email']
        }
        db_interface.commit(query, args)
        bizID = db_interface.retrieve_one("select businessID from businesses order by businessID desc limit 1;", None)

        query = "insert into cities (name, businessID) values (:name, :businessID)" # Add city to city database
        args = { 'name'       : fullAdd,
                 'businessID' : bizID[0]
        }
        db_interface.commit(query, args)
        
        for keyword in b[10].split(','):
            query = "insert into businessKeywords (businessID, keyword) values (:businessID, :keyword)"  # Add keywords to keywords database
            args = {
                'businessID' : bizID[0],
                'keyword'    : keyword
            }
            db_interface.commit(query, args)

        self.deleteBusiness(businessID)     # Remove business from unconfirmedBusinesses

    def deleteBusiness(self, businessID):
        query       = 'delete from unconfirmed_businesses where id = :biz_id '
        args        = {'biz_id' : businessID}
        db_interface.commit(query, args)

    def getPassword(self):
        query   = "select * from password"
        password = db_interface.retrieve_one(query, None)
        if not password:
            return None
        else:
            return password[0]

