# Python packages
import os

# Flask packages
from flask              import Flask
from flask_sqlalchemy   import SQLAlchemy
from functools          import wraps
from flask_session      import Session

session = Session()
db      = SQLAlchemy()

# Hitch packages
from . import config, db_interface, routes, spaces_interface

def create_app():

    # Create app object
    application = Flask(__name__)

    # Config
    application.config.from_object(config.DefaultConfig)
    try:
        from .instance import prod_config
        application.config.from_object(prod_config.ProductionConfig)
    except:
        from .instance import dev_config
        application.config.from_object(dev_config.DevelopmentConfig)



    # Initialize db
    db.init_app(application)

    with application.app_context():
        # Register blueprints
        application.register_blueprint(db_interface.bp)
        application.register_blueprint(routes.bp)
        application.register_blueprint(spaces_interface.bp)

        # Set space location correctly
        routes.space_location       = application.config['SPACES_RESOURCE_URL']
        routes.space_location_pdf   = ''.join([application.config['PDF_VIEW_PREFIX'], routes.space_location])

    return application
