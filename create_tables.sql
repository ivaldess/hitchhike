create table businesses(
    businessID integer primary key autoincrement,
    name varchar(255),
    website varchar(255),
    pdf varchar(255),
    description varchar(255),
    address varchar(255),
    phone_number varchar(255),
    logo varchar(255),
    keywords varchar(255),
    email varchar(255)
);

create table businessKeywords(
    businessID int,
    keyword varchar(255),
    PRIMARY KEY (businessID, keyword)
);

create table unconfirmed_businesses(
    name varchar(255),
    website varchar(255),
    pdf varchar(255),
    description varchar(255),
    address varchar(255),
    city varchar(255),
    state varchar(255),
    country varchar(255),
    phone_number varchar(255),
    logo varchar(255),
    keywords varchar(255),
    id integer primary key autoincrement,
    email varchar(255)
);

create table cities(
    name varchar(255),
    businessID int primary_key
);

create table password(
    password varchar(255)
);
